# Magpie and Egg OOP Challenge

Modify the code below to meet the following requirements

## Requirements
1) Magpie needs to implement IBird
2) A magpie can lay an egg
3) Only a magpie can lay an egg, no other IBirds can
4) An egg can hatch a magpie
5) An egg cannot be hatched twice - this needs to throw an Exception.

---

## The code 

```cs
using System;

public interface IBird
{
    Egg Lay();
}

public class Magpie
{

}

public class Egg
{
    public Egg(Func<IBird> createBird)
    {
        // TODO
    }

    public IBird Hatch()
    {
        //TODO
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        var magpie1 = new Magpie();
        var egg = magpie1.Lay();
        var childMagpie = egg.Hatch();
    }
}
```
