using System;

public class Program
{
    public static void Main(string[] args)
    {
        var magpie1 = new Magpie();
        var egg = magpie1.Lay();
        var childMagpie = egg.Hatch();	
    }
}

public interface IBird
{
    Egg Lay();
}

public class Magpie : IBird
{
    public Egg Lay()
    {
        return new Egg(new Func<Magpie>(() => new Magpie()));
    }
}

public class Egg
{
    private Func<IBird> newBird;
    private bool hatched;

    public Egg(Func<IBird> createBird)
    {
        newBird = createBird;
    }

    public IBird Hatch()
    {
        if (!hatched) 
        {
            hatched = true;
            return newBird();
        }
        else 
        {
            throw new AlreadyHatchedException("This egg has already hatched");
        }
    }
}

public class AlreadyHatchedException : System.Exception
{
    public AlreadyHatchedException() : base() { }
    public AlreadyHatchedException(string message) : base(message) { }
}
